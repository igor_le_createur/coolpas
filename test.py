from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from menu.models import Menu


class MenuTests(APITestCase):
    def test_menu(self):
        # Menu.objects.create(content='test', frcontent='BONjour', subtitle='fdsfsd', frsubtitle='fdf', language='eng',
        #                     fr='fr', eng='eng', footer=True, menu=False, order=3, type='item',
        #                     link='http://127.0.0.1:8000/api/v1/menu/all/', image='test.jpeg')

        url = reverse('menu')
        data = {'content': "test2"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 405)
        self.assertEqual(Menu.objects.count(),1)
        self.assertEqual(Menu.objects.get().content, 'test2')
