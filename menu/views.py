import json

import mixins as mixins
import xmltodict as xmltodict
from django.contrib.sites import requests
from django.shortcuts import render
from rest_framework import generics, viewsets, mixins
from rest_framework.views import APIView
from requests import Response
from django.http import JsonResponse
from menu.models import Menu
from menu.sereializers import *


class MenuCreateView(generics.CreateAPIView):
    serializer_class = MenuListSerializer


class MenuView(generics.ListAPIView):
    serializer_class = MenuListSerializer
    queryset = Menu.objects.all()


class MenuDetailView(generics.RetrieveAPIView):
    queryset = Menu.objects.all()
    serializer_class = MenuListSerializer
#
#
# class MenuUpdateView(generics.UpdateAPIView):
#     queryset = Menu.objects.all()
#     serializer_class = MenuListSerializer
#
#
# class MenuDeleteView(generics.DestroyAPIView):
#     queryset = Menu.objects.all()
#     serializer_class = MenuListSerializer
