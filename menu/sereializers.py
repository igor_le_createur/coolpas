from rest_framework import serializers

from menu.models import *


class MenuListSerializer(serializers.ModelSerializer):
    content = serializers.SerializerMethodField()

    class Meta:
        model = Menu
        fields = "id", "content", "footer", "menu", "order", "type", "link", "image"

    def get_content(self, objeсt):
        return {objeсt.eng: {'title': objeсt.content, 'subtitle': objeсt.subtitle}}, {
            objeсt.fr: {'title': objeсt.frcontent, 'subtitle': objeсt.frsubtitle}}
