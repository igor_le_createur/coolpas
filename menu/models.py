from django.db import models


# Create your models here.

class Menu(models.Model):
    content = models.JSONField()
    frcontent = models.JSONField(default=dict)
    subtitle = models.JSONField(default=dict)
    frsubtitle = models.JSONField(default=dict)
    language = models.CharField(default='-', max_length=3)
    fr = models.CharField(default='fr', max_length=3)
    eng = models.CharField(default='eng', max_length=3)
    footer = models.BooleanField(default=False)
    menu = models.BooleanField(default=False)
    order = models.PositiveIntegerField(("order"))
    type = models.CharField(choices=(('item', 'item'), ('section', 'section')), max_length=20)
    link = models.URLField()
    image = models.JSONField(default=list)



# class Meta:
#     verbose_name_plural = 'list of the menu'
