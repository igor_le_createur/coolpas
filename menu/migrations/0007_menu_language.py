# Generated by Django 4.1.3 on 2023-03-09 13:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0006_menu_image_alter_menu_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='language',
            field=models.CharField(default='-', max_length=3),
        ),
    ]
