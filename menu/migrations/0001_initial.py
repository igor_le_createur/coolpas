# Generated by Django 4.1.3 on 2023-03-06 10:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('content', models.CharField(max_length=50, verbose_name='Name')),
                ('footer', models.BooleanField(default=False)),
                ('menu', models.BooleanField(default=False)),
                ('order', models.PositiveIntegerField(verbose_name='order')),
                ('type', models.CharField(choices=[('HEADER', 'Заголовок'), ('LINK', 'Ссылка')], max_length=20)),
                ('link', models.URLField()),
            ],
        ),
    ]
